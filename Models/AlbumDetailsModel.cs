using System.Collections.Generic;
using System.Linq;

namespace APITeste.Models
{
    public class AlbumDetailsModel
    {

        public string ArtistName { get; set; }
        public string Name { get; set; }
        public IEnumerable<MusicModel> Musics { get; set; }
    }
}