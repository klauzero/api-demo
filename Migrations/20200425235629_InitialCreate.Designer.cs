﻿// <auto-generated />
using APITeste.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace APITeste.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20200425235629_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.3");

            modelBuilder.Entity("APITeste.Data.Entities.ArtistEntity", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("Artists");
                });

            modelBuilder.Entity("APITeste.Data.Entities.DiscoEntity", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("TEXT");

                    b.Property<string>("ArtistId")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("ArtistId");

                    b.ToTable("Discos");
                });

            modelBuilder.Entity("APITeste.Data.Entities.MusicEntity", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("TEXT");

                    b.Property<string>("DiscoId")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("DiscoId");

                    b.ToTable("Musics");
                });

            modelBuilder.Entity("APITeste.Data.Entities.DiscoEntity", b =>
                {
                    b.HasOne("APITeste.Data.Entities.ArtistEntity", "Artist")
                        .WithMany("Discos")
                        .HasForeignKey("ArtistId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("APITeste.Data.Entities.MusicEntity", b =>
                {
                    b.HasOne("APITeste.Data.Entities.DiscoEntity", "Disco")
                        .WithMany("Musics")
                        .HasForeignKey("DiscoId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
