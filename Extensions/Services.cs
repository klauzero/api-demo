using APITeste.Data;
using APITeste.Data.Entities;
using APITeste.Data.Repositories;
using APITeste.Interfaces;
using APITeste.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace APITeste.Extensions
{
    public static class Services
    {
        public static void ConfigureDataContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlite("Data Source=sapiteste.db"));
        }

        public static void ConfigureRepositories(this IServiceCollection services)
        {
            services.AddTransient<IRepositoryWrapper, RepositoryWrapper>();
        }

        public static void ConfigureAutoMapper(this IServiceCollection services)
        {

            services.AddAutoMapper(typeof(Startup));
            IMapper mapper = new MapperConfiguration(config =>
            {
                config.AllowNullDestinationValues = false;
                config.CreateMap<MusicEntity, MusicModel>();
                config.CreateMap<DiscoEntity, AlbumDetailsModel>()
                    .ForMember(a => a.ArtistName, opt => opt.MapFrom(x => x.Artist.Name))
                    .ForMember(a => a.Musics, opt => opt.MapFrom(x => x.Musics));
            }).CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}