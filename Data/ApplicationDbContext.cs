using APITeste.Data.Entities;
using APITeste.Data.Mapping;
using Microsoft.EntityFrameworkCore;

namespace APITeste.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            // this.Database.EnsureCreated();
        }

        public DbSet<ArtistEntity> Artists { get; set; }
        public DbSet<DiscoEntity> Discos { get; set; }
        public DbSet<MusicEntity> Musics { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ArtistEntity>(new ArtistMap().Configure);
            builder.Entity<DiscoEntity>(new DiscoMap().Configure);
            builder.Entity<MusicEntity>(new MusicMap().Configure);
        }
    }
}