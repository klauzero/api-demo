using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APITeste.Data.Entities
{
    public class DiscoEntity
    {
        public string Id { get; set; }
        public string Name { get; set;}
        public string ArtistId { get; set; }
        public virtual ArtistEntity Artist { get; set; }
        public virtual ICollection<MusicEntity> Musics { get; set; }
    }
}