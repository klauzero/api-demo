using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APITeste.Data.Entities
{
    public class MusicEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string DiscoId { get; set; }
        public virtual DiscoEntity Disco { get; set; }
    }
}