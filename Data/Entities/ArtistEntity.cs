using System.Collections.Generic;

namespace APITeste.Data.Entities
{
    public class ArtistEntity
    {
        public string Id { get; set; }
        public string Name { get; set;}
        public virtual ICollection<DiscoEntity> Discos { get; set; }
    }
}