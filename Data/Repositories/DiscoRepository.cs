using APITeste.Data.Entities;
using APITeste.Interfaces;

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APITeste.Data.Repositories
{
    public class DiscoRepository : RepositoryBase<DiscoEntity>, IDiscoRepository
    {
        public DiscoRepository(ApplicationDbContext applicationDbContext)
            : base(applicationDbContext)
        {

        }
        public DiscoEntity GetById(string id)
        {
            return FindByCondition(x => x.Id == id)
                .Include(x => x.Artist)
                .Include(x => x.Musics)
                .FirstOrDefault() ?? new DiscoEntity { };
        }
    }
}