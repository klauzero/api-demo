using System;
using System.Linq;
using System.Linq.Expressions;
using APITeste.Interfaces;

namespace APITeste.Data.Repositories
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        private ApplicationDbContext ApplicationDbContext { get; set; }
        public RepositoryBase(ApplicationDbContext applicationDbContext)
        {
            ApplicationDbContext = applicationDbContext;
        }

        public IQueryable<T> FindAll()
        {
            return ApplicationDbContext.Set<T>();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return ApplicationDbContext.Set<T>().Where(expression);
        }

        public void Create(T entity) => _ = ApplicationDbContext.Set<T>().AddAsync(entity);

        public void Update(T entity) => _ = ApplicationDbContext.Set<T>().Update(entity);

        public void Delete(T entity) => _ = ApplicationDbContext.Set<T>().Remove(entity);
    }
}