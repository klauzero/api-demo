using APITeste.Interfaces;

namespace APITeste.Data.Repositories
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private IDiscoRepository discoRepository;
        public RepositoryWrapper(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }
        public IDiscoRepository Disco
        {
            get
            {
                if (discoRepository == null)
                {
                    discoRepository = new DiscoRepository(_applicationDbContext);
                }
                return discoRepository;
            }
        }
    }
}