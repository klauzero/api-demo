using APITeste.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace APITeste.Data.Mapping
{
    public class DiscoMap : IEntityTypeConfiguration<DiscoEntity>
    {
        public void Configure(EntityTypeBuilder<DiscoEntity> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.Artist).WithMany(x => x.Discos).HasForeignKey(x => x.ArtistId).OnDelete(DeleteBehavior.Cascade);
        }
    }
}