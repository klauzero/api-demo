using APITeste.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace APITeste.Data.Mapping
{
    public class MusicMap : IEntityTypeConfiguration<MusicEntity>
    {
        public void Configure(EntityTypeBuilder<MusicEntity> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.Disco).WithMany(x => x.Musics).HasForeignKey(x => x.DiscoId).OnDelete(DeleteBehavior.Cascade);
        }
    }
}