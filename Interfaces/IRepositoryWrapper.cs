namespace APITeste.Interfaces
{
    public interface IRepositoryWrapper
    {
         IDiscoRepository Disco { get; }
    }
}