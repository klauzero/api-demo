using APITeste.Data.Entities;

namespace APITeste.Interfaces
{
    public interface IDiscoRepository : IRepositoryBase<DiscoEntity>
    {
         DiscoEntity GetById(string id);
    }
}