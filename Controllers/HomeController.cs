﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APITeste.Interfaces;
using APITeste.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace APITeste.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        private readonly IRepositoryWrapper _repository;
        private readonly IMapper _mapper;
        public HomeController(IRepositoryWrapper repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public object Index([FromQuery]string id)
        {
            return _mapper.Map<AlbumDetailsModel>(_repository.Disco.GetById(id));
        }

    }
}
